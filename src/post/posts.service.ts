import { Injectable, Inject } from '@nestjs/common';
import { Post } from './post.entity';
import { Commentary } from 'src/commentary/commentary.entity';

/**
 * Service de la table POST 
 */
@Injectable()
export class PostsService {
  constructor(@Inject('POSTS_REPOSITORY') private readonly POSTS_REPOSITORY: typeof Post) { }

  /**
   * Récupère tous les posts de la bdd
   */
  async findAllPosts(): Promise<Post[]> {
    return await this.POSTS_REPOSITORY.findAll();
  }

  /**
   * Récupère un post via son id, dans la bdd
   * @param id l'id du Post à récupérer
   */
  async findOnePost(id: number): Promise<Post> {
    return await this.POSTS_REPOSITORY.findByPk(id, { include: [Commentary] });
  }

  /**
 * Ajoute un post en bdd
 * @param post l'objet Post à ajouté en base
 */
  async createPost(post: Post) {
    return await this.POSTS_REPOSITORY.create(post);
  }

  /**
   * Met à jour un post en bdd
   * @param post l'objet Post à modifié en bdd
   */
  async updatePost(post: Post) {
    const res = await this.POSTS_REPOSITORY.update(post, { where: { id: post.id } });
    const postUpdated: Post = await this.findOnePost(post.id);
    return { 'updated': (res[0] !== 0), 'post': postUpdated };
  }

  /**
   * Supprime un post de la bdd
   * @param id l'id du Post à supprimer en bdd
   */
  async removeOnePost(id: number): Promise<object> {
    let post = await this.findOnePost(id);
    post.destroy();
    post = await this.findOnePost(id);
    return { 'isDelete': (post === null), 'id': id };
  }
}