import { Controller, Get, Param, Post, Body, Put, Delete, Request } from '@nestjs/common';
import { UsersService } from './users.service';
import { User } from './user.entity';

@Controller('users')
export class UsersController {
  
  constructor(private readonly usersService: UsersService){}

  /**
   * Fait appelle à la fonction `findAllUsers` du service `Users`
   */
  @Get() 
  findAllUsers() {
    return this.usersService.findAllUsers();
  }

  /**
   * Fait appelle à la fonction `findOneUser` du service `Users`
   * @param id l'id de l'utilisateur à récupérer
   */
  @Get(':id')
  findOneUser(@Param('id') id: string) {
    return this.usersService.findOneUser(parseInt(id))
  }

  /**
   * Fait appelle à la fonction `createUser` du service `Users`
   * @param user l'objet `User` à crée
   */
  @Post()
  createUser(@Body() user: User) {
    return this.usersService.createUser(user);
  }

  /**
   * Fait appelle à la fonction `updateUser` du service `Users`
   * @param user l'objet `User` à modifié
   */
  @Put()
  updateUser(@Body() user: User) {
    return this.usersService.updateUser(user);
  }

  /**
   * Fait appelle à la fonction `removeOneUser` du service `Users`
   * @param id l'id de l'utilisateur à supprimer
   */
  @Delete(':id')
  removeOneUser(@Param('id') id: string) {
    return this.usersService.removeOneUser(parseInt(id));
  }
}