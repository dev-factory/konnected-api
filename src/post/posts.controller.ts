import { Controller, Get, Delete, Param, Put, Body, Post } from '@nestjs/common';
import { PostsService } from './posts.service';
import { Post as PostEntity } from './post.entity';

@Controller('posts')
export class PostsController {

  constructor(private readonly postsService: PostsService){}

  /**
   * Fait appelle la fonction `findAllPosts` du service `Posts`.
   */
  @Get()
  findAllPosts() {
    return this.postsService.findAllPosts();
  }

  /**
   * Fait appelle à la fonction `findOnePost` du service `Posts`
   * @param id l'id du commentaire à récupérer
   */
  @Get(':id')
  findOnePost(@Param('id') id: string) {
    return this.postsService.findOnePost(parseInt(id));
  }

  /**
   * Fait appelle à la fonction `createPost` du service `Posts`
   * @param post l'objet `Post` à crée
   */
  @Post()
  createPost(@Body() post: PostEntity) {
    return this.postsService.createPost(post);
  }

  /**
   * Fait appelle à la fonction `updatePost` du service `Posts`
   * @param post l'objet `Post` à modifié
   */
  @Put()
  updatePost(@Body() post: PostEntity) {
    return this.postsService.updatePost(post);
  }

  /**
   * Fait appelle à la fonction `removeOnePost` du service `Posts`
   * @param id l'id du commentaire à supprimer
   */
  @Delete(':id')
  removeOnePost(@Param('id') id: string) {
    return this.postsService.removeOnePost(parseInt(id))
  }
}