import { Injectable, Inject } from '@nestjs/common';
import { Commentary } from './commentary.entity';

/**
 * Service de la table COMMENTARY
 */
@Injectable()
export class CommentariesService {
  constructor(@Inject('COMMENTARIES_REPOSITORY') private readonly COMMENTARIES_REPOSITORY: typeof Commentary) {}

  /**
   * Récupère tous les commentaires de la bdd
   */
  async findAllCommentaries(): Promise<Commentary[]> {
    return await this.COMMENTARIES_REPOSITORY.findAll();
  }

  /**
   * Récupère un commentaire via son id, dans la bdd
   * @param id l'id du Commentary à récuperer
   */
  async findOneCommentary(id: number): Promise<Commentary> {
    return await this.COMMENTARIES_REPOSITORY.findByPk(id);
  }

  /**
   * Ajoute un commentaire en bdd
   * @param commentary l'objet Commentary à ajouté en base
   */
  async createCommentary(commentary: Commentary) {
    return await this.COMMENTARIES_REPOSITORY.create(commentary);
  }

  /**
   * Met à jour un commentaire en bdd
   * @param commentary l'objet Commentary à modifié en bdd
   */
  async updateCommentary(commentary: Commentary) {
    const res = await this.COMMENTARIES_REPOSITORY.update(commentary, { where : { id: commentary.id }});
    const commentaryUpdated: Commentary = await this.findOneCommentary(commentary.id);
    return { 'updated': (res[0] !== 0), 'commentary': commentaryUpdated };
  }

  /**
   * Supprime un commentaire de la bdd
   * @param id l'id du Commentary à supprimer en bdd
   */
  async removeOneCommentary(id: number): Promise<object> {
    let commentary = await this.findOneCommentary(id);
    commentary.destroy();
    commentary = await this.findOneCommentary(id);
    return { 'isDelete': (commentary === null), 'id': id };
  }
  
}