import { Table, Column, Model, PrimaryKey, AutoIncrement, HasMany } from 'sequelize-typescript';
import { Commentary } from 'src/commentary/commentary.entity';
import { Post } from 'src/post/post.entity';

/**
 * Entité de la table `User`
 */
@Table({freezeTableName: true, timestamps: false})
export class User extends Model<User> {

  @PrimaryKey
  @AutoIncrement
  @Column
  id: number

  @Column
  firstname: string;

  @Column
  lastname: string;

  @Column
  email: string;

  @Column
  birthdate: Date;

  @HasMany(() => Commentary)
  commentaries: Commentary[];

  @HasMany(() => Post)
  posts: Post[];
}