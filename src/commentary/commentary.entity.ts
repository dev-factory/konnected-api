import { Table, Column, Model, PrimaryKey, AutoIncrement, BelongsTo, ForeignKey } from 'sequelize-typescript';
import { User } from 'src/user/user.entity';
import { Post } from 'src/post/post.entity';

/**
 * Entité de la table `Commentary`
 */
@Table({freezeTableName: true, timestamps: true})
export class Commentary extends Model<Commentary> {
  
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  message: string;

  @Column
  like: number;

  @ForeignKey(() => User)
  @Column
  userId: number;

  @ForeignKey(() => Post)
  @Column
  postId: number;

  @BelongsTo(() => User)
  user: User;

  @BelongsTo(() => Post)
  post: Post;
}