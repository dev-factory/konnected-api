import { PrimaryKey, AutoIncrement, Model, Column, ForeignKey, Table, HasMany, BelongsTo } from 'sequelize-typescript';
import { User } from 'src/user/user.entity';
import { Commentary } from 'src/commentary/commentary.entity';

/**
 * Entité de la table `Post`
 */
@Table({freezeTableName: true, timestamps: true})
export class Post extends Model<Post> {

  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  content: string;

  @ForeignKey(() => User)
  @Column
  userId: number;

  @HasMany(() => Commentary)
  commentaries: Commentary[]

  @BelongsTo(() => User)
  user: User
}