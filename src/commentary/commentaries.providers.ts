import { Commentary } from './commentary.entity';

export const commentariesProviders = [
  {
    provide: 'COMMENTARIES_REPOSITORY',
    useValue: Commentary,
  },
];