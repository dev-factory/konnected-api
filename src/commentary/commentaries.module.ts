import { Module } from '@nestjs/common';
import { CommentariesController } from './commentaries.controller';
import { CommentariesService } from './commentaries.service';
import { commentariesProviders } from './commentaries.providers';
import { DatabaseModule } from 'src/database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [CommentariesController],
  providers: [
    CommentariesService,
    ...commentariesProviders
  ],
})
export class CommentariesModule {};