import { Injectable, Inject } from '@nestjs/common';
import { User } from './user.entity';
import { Commentary } from 'src/commentary/commentary.entity';
import { Post } from 'src/post/post.entity';

/**
 * Service de la table USER
 */
@Injectable()
export class UsersService {
  constructor(@Inject('USERS_REPOSITORY') private readonly USERS_REPOSITORY: typeof User) { }

  /**
   * Récupère tous les utilisateurs de la bdd
   */
  async findAllUsers(): Promise<User[]> {
    return await this.USERS_REPOSITORY.findAll();
  }

  /**
   * Récupère un utilisateur via son id, dans la bdd
   * @param id l'id du User à récupérer
   */
  async findOneUser(id: number): Promise<User> {
    return await this.USERS_REPOSITORY.findByPk(id, {include: [Commentary, Post]});
  }

  /**
   * Ajoute un utilisateur en bdd
   * @param user l'objet User à ajouté en base
   */
  async createUser(user: User): Promise<User> {
    return await this.USERS_REPOSITORY.create(user);
  }

  /**
   * Met à jour un utilisateur en bdd
   * @param user l'objet User à modifié en bdd
   */
  async updateUser(user: User): Promise<object> {
    const res = await this.USERS_REPOSITORY.update(user, { where: { id: user.id } });
    const userUpdated: User = await this.findOneUser(user.id);
    return { 'updated': (res[0] !== 0), 'user': userUpdated };
  }

  /**
   * Supprime un utilisateur de la bdd
   * @param id l'id de l'User à supprimer en bdd
   */
  async removeOneUser(id: number): Promise<object> {
    let user = await this.findOneUser(id);
    user.destroy()
    user = await this.findOneUser(id);
    return { 'isDelete': (user === null), 'id': id };
  }
}