import { Controller, Get, Param, Body, Post, Put, Delete } from '@nestjs/common';
import { CommentariesService } from './commentaries.service';
import { Commentary } from './commentary.entity';

@Controller('commentaries')
export class CommentariesController {

  constructor(private readonly commentariesService: CommentariesService){}

  /**
   * Fait appelle la fonction `findAllCommentaries` du service `Commentaries`.
   */
  @Get()
  findAllCommentaries() {
    return this.commentariesService.findAllCommentaries();
  }

  /**
   * Fait appelle à la fonction `findOneCommentary` du service `Commentaries`
   * @param id l'id du commentaire à récupérer
   */
  @Get(':id')
  findOneCommentary(@Param('id') id: string) {
    return this.commentariesService.findOneCommentary(parseInt(id));
  }

  /**
   * Fait appelle à la fonction `createCommentary` du service `Commentaries`
   * @param commentary l'objet `Commentary` à crée
   */
  @Post()
  createCommentary(@Body() commentary: Commentary) {
    return this.commentariesService.createCommentary(commentary);
  }

  /**
   * Fait appelle à la fonction `updateCommentary` du service `Commentaries`
   * @param commentary l'objet `Commentary` à modifié
   */
  @Put()
  updateCommentary(@Body() commentary: Commentary) {
    return this.commentariesService.updateCommentary(commentary);
  }

  /**
   * Fait appelle à la fonction `removeOneCommentary` du service `Commentaries`
   * @param id l'id du commentaire à supprimer
   */
  @Delete(':id')
  removeOneCommentary(@Param('id') id: string) {
    return this.commentariesService.removeOneCommentary(parseInt(id))
  }
}