import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './user/users.module';
import { CommentariesModule } from './commentary/commentaries.module';
import { PostsModule } from './post/posts.module';

@Module({
  imports: [UsersModule, CommentariesModule, PostsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
