import { Sequelize } from 'sequelize-typescript';
import { User } from '../user/user.entity';
import { Commentary } from '../commentary/commentary.entity';
import { Post } from '../post/post.entity';

export const databaseProviders = [
  {
    provide: 'SEQUELIZE',
    useFactory: async () => {
      const sequelize = new Sequelize({
        dialect: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: '',
        database: 'konnected',
      });
      sequelize.addModels([Post, User, Commentary]);
      await sequelize.sync();
      return sequelize;
    },
  },
];